import 'package:app_deer/flutter_deer/widgets/my_app_bar.dart';
import 'package:flutter/material.dart';

class NotFoundPage extends StatelessWidget {
  const NotFoundPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: MyAppBar(
        title: Text('空页面'),
      ),
      body: Center(
        child: Text('错误页面'),
      ),
    );
  }
}
