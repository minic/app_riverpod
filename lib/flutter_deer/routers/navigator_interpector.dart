import 'package:app_deer/flutter_deer/common/deer_storage.dart';
import 'package:app_deer/flutter_deer/modules/login/login_route.dart';
import 'package:app_deer/flutter_deer/routers/navigator_utils.dart';
import 'package:flutter/material.dart';

typedef InterpectorNext = void Function();

mixin NavigatorInterpector {
  /// 下一个拦截器
  NavigatorInterpector? next;

  void interpector(BuildContext context, InterpectorNext handler) {
    if (next == null) handler();
    next!.interpector(context, handler);
  }
}

class LoginInterpector with NavigatorInterpector {
  @override
  void interpector(BuildContext context, InterpectorNext handler) {
    /// 判断是否登录
    if (DeerStorage.isLogin) {
      handler();
      return;
    }
    NavigatorUtils.push(context, LoginRouter.login, resultCallback: (value) {
      if (value != null && DeerStorage.isLogin) {
        handler();
        return;
      }
    });
  }
}
