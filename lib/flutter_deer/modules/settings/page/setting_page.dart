import 'package:app_deer/flutter_deer/common/config_providers.dart';
import 'package:app_deer/flutter_deer/common/user_provider.dart';
import 'package:app_deer/flutter_deer/modules/settings/widgets/setting_item.dart';
import 'package:app_deer/flutter_deer/routers/navigator_utils.dart';
import 'package:app_deer/flutter_deer/utils/dialog_utils.dart';
import 'package:app_deer/flutter_deer/widgets/alert_view.dart';
import 'package:app_deer/flutter_deer/widgets/my_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SettingPage extends ConsumerStatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  ConsumerState<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends ConsumerState<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        title: Text('设置'),
      ),
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
          children: [
            SettingItem(title: '退出登录', onTap: logout),
            SettingItem(title: 'Github', content: 'follow & star', onTap: toGithub),
            SettingItem(
                title: 'Light',
                onTap: () {
                  ref.read(ConfigProviders.config.notifier).changeDarkMode(ThemeMode.light);
                }),
            SettingItem(
                title: 'Dark',
                onTap: () {
                  ref.read(ConfigProviders.config.notifier).changeDarkMode(ThemeMode.dark);
                }),
          ],
        ),
      ),
    );
  }

  void logout() => DialogUtils.show(context, builder: (context) {
        return AlertView(
          title: '提示',
          message: '您确定要退出登录吗？',
          onTap: () {
            ref.read(UserProviders.userInfo.notifier).logOut();
          },
        );
      });

  void toGithub() => NavigatorUtils.pushToWeb(context, 'Github', 'https://github.com/MengLiMing/app_deer');
}
