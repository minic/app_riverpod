import 'package:flutter/material.dart';

Widget func() {
  return SizedBox(
    child: GestureDetector(
      onTap: () {},
      behavior: HitTestBehavior.opaque,
      child: const AnimationTapScaleWidget(child: Text('Andi')),
    ),
  );
}

class AnimationTapScaleWidget extends StatefulWidget {
  const AnimationTapScaleWidget({super.key, required this.child});
  final Widget child;

  @override
  State<AnimationTapScaleWidget> createState() => _AnimationTapScaleWidgetState();
}

class _AnimationTapScaleWidgetState extends State<AnimationTapScaleWidget> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: (event) async {
        _controller.forward();
        await Future.delayed(const Duration(milliseconds: 200));
        _controller.reverse();
      },
      behavior: HitTestBehavior.opaque,
      child: ScaleTransition(
        scale: Tween(begin: 1.0, end: 0.9).animate(_controller),
        child: widget.child,
      ),
    );
  }
}
